/**
 * Данный сервис класс является посредником
 * между репозиторием и контроллером,
 * в данном классе проверяются входные данные и передаются в контроллер
 */

package ru.ovechkin.tm.service;

import ru.ovechkin.tm.api.service.ICommandService;
import ru.ovechkin.tm.model.Command;
import ru.ovechkin.tm.repository.CommandRepository;

public class CommandService implements ICommandService {

    private final CommandRepository commandRepository;

    public CommandService(CommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    public String[] getArgs() {
        return commandRepository.getArgs();
    }

}