/**
 * В данном классе находится главная петля приложения
 * Перенаправляет все вещи на контроллер
 */

package ru.ovechkin.tm.bootstrap;

import ru.ovechkin.tm.api.controller.ICommandController;
import ru.ovechkin.tm.api.controller.IProjectController;
import ru.ovechkin.tm.api.controller.ITaskController;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.api.service.ICommandService;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.controller.CommandController;
import ru.ovechkin.tm.controller.ProjectController;
import ru.ovechkin.tm.controller.TaskController;
import ru.ovechkin.tm.repository.CommandRepository;
import ru.ovechkin.tm.repository.ProjectRepository;
import ru.ovechkin.tm.repository.TaskRepository;
import ru.ovechkin.tm.service.CommandService;
import ru.ovechkin.tm.service.ProjectService;
import ru.ovechkin.tm.service.TaskService;
import ru.ovechkin.tm.util.TerminalUtil;

public final class Bootstrap {

    private final CommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String[] args) {
        commandController.displayWelcome();
        if (parseArgs(args)) System.exit(0);
        process();
    }

    private void process() {
        String command = "";
        while (!CmdConst.CMD_EXIT.equals(command)) {
            System.out.print("Enter command: ");
            command = TerminalUtil.nextLine();
            parseCommand(command);
            parseArg(command);
            System.out.println();
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        parseCommand(arg);
        return true;
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ARG_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ARG_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.ARG_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARG_ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.ARG_COMMANDS:
                commandController.showCommands();
                break;
        }
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CmdConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case CmdConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case CmdConst.CMD_HELP:
                commandController.showHelp();
                break;
            case CmdConst.CMD_INFO:
                commandController.showInfo();
                break;
            case CmdConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case CmdConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case CmdConst.CMD_TASK_LIST:
                taskController.showTasks();
                break;
            case CmdConst.CMD_TASK_CREATE:
                taskController.createTask();
                break;
            case CmdConst.CMD_TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CmdConst.CMD_PROJECT_LIST:
                projectController.showProjects();
                break;
            case CmdConst.CMD_PROJECT_CREATE:
                projectController.createProjects();
                break;
            case CmdConst.CMD_PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CmdConst.CMD_EXIT:
                commandController.exit();
                break;
            case CmdConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case CmdConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case CmdConst.TASK_SHOW_BY_NAME:
                taskController.showTaskByName();
                break;
            case CmdConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CmdConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CmdConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CmdConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CmdConst.TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case CmdConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case CmdConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CmdConst.PROJECT_SHOW_BY_NAME:
                projectController.showProjectByName();
                break;
            case CmdConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CmdConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CmdConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CmdConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CmdConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
        }
    }

}