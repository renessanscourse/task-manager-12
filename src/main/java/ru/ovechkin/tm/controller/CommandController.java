package ru.ovechkin.tm.controller;

import ru.ovechkin.tm.api.controller.ICommandController;
import ru.ovechkin.tm.api.service.ICommandService;
import ru.ovechkin.tm.model.Command;
import ru.ovechkin.tm.util.NumberUtil;

/**
 * Контроллер отвечает за ввод и вывод
 */

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    public void displayWelcome() {
        System.out.println();
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0");
    }

    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME:\tOvechkin Roman");
        System.out.println("E-MAIL:\troman@ovechkin.ru");
    }

    public void showCommands() {
        final String[] commands = commandService.getCommands();
        for (final String command : commands) System.out.println(command);
    }

    public void showArguments() {
        final String[] arguments = commandService.getArgs();
        for (final String argument : arguments) System.out.println(argument);
    }

    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

    public void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory (bytes): " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM (bytes): " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM (bytes): " + usedMemoryFormat);
    }

    public void exit() {
        System.exit(0);
    }

}