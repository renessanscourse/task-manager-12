/**
 * Данный Api-репозиторий-Интерфейс описывает то,
 * как должен выглядеть соответствующий сервис класс
 */

package ru.ovechkin.tm.api.repository;

import java.util.List;

import ru.ovechkin.tm.model.Project;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findById(String id);

    Project findByIndex(Integer index);

    Project findByName(String name);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project removeByName(String name);

}