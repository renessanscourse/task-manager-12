/**
 * Данный Api-репозиторий-Интерфейс описывает то,
 * как должен выглядеть соответствующий сервис класс
 */

package ru.ovechkin.tm.api.repository;

import ru.ovechkin.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findById(String id);

    Task findByIndex(Integer index);

    Task findByName(String name);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task removeByName(String name);

}