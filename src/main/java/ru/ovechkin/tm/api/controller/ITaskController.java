/**
 * Данный Api-контроллер-Интерфейс описывает то,
 * как должен выглядеть соответствующий сервис класс
 */

package ru.ovechkin.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByName();

    void updateTaskById();

    void updateTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void removeTaskByName();

}