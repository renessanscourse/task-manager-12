/**
 * Данный Api-контроллер-Интерфейс описывает то,
 * как должен выглядеть соответствующий сервис класс
 */

package ru.ovechkin.tm.api.controller;

public interface ICommandController {

    void displayWelcome();

    void showVersion();

    void showAbout();

    void showCommands();

    void showArguments();

    void showHelp();

    void showInfo();

    void exit();

}