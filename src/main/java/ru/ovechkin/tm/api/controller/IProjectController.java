/**
 * Данный Api-контроллер-Интерфейс описывает то,
 * как должен выглядеть соответствующий сервис класс
 */

package ru.ovechkin.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProjects();

    void showProjectById();

    void showProjectByIndex();

    void showProjectByName();

    void updateProjectById();

    void updateProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

}