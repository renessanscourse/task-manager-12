/**
 * Данный Api-сервис-Интерфейс описывает то,
 * как должен выглядеть соответствующий сервис класс
 */

package ru.ovechkin.tm.api.service;

import ru.ovechkin.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void create(String name);

    void create(String name, String description);

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void removeTask(final Task task);

    void removeAllTasks();

    Task findTaskById(String id);

    Task findTaskByIndex(Integer index);

    Task findTaskByName(String name);

    Task updateTaskById(String id, String name, String description);

    Task updateTaskByIndex(Integer index, String name, String description);

    Task removeTaskById(String id);

    Task removeTaskByIndex(Integer index);

    Task removeTaskByName(String name);

}
