/**
 * Данный Api-сервис-Интерфейс описывает то,
 * как должен выглядеть соответствующий сервис класс
 */

package ru.ovechkin.tm.api.service;

import ru.ovechkin.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void create(String name);

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void removeProject(Project project);

    void removeAllProjects();

    Project findProjectById(String id);

    Project findProjectByIndex(Integer index);

    Project findProjectByName(String name);

    Project updateProjectById(String id, String name, String description);

    Project updateProjectByIndex(Integer index, String name, String description);

    Project removeProjectById(String id);

    Project removeProjectByIndex(Integer index);

    Project removeProjectByName(String name);

}