# PROJECT INFO

**TASK-MANAGER**

# DEVELOPER INFO

**NAME:** Ovechkin Roman

**E-MAIL:** roman@ovechkin.ru

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

# SCREENSHOTS

| Описание | Ссылка |
|:----|:----|
| Вывод в help новых коммнд | https://yadi.sk/i/Ng7kmTUaXg7HuA | 
| Создание, вывод задач по id | https://yadi.sk/i/BXxhihUcJCV1Zw  |
| Вывод задач по index, обновление задач | https://yadi.sk/i/kjFTi4fPM2bb8Q |
| Удаление задач | https://yadi.sk/i/tstPvSk5xBWKCA |
| Вывод проектов | https://yadi.sk/i/wkxryMt9YPiXAw |
| Изменение проектов | https://yadi.sk/i/6R6EendrAKCJ_Q |
| Удаление проектов | https://yadi.sk/i/fohkxnfFoUPS_g |